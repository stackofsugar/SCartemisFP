from flask import Blueprint, request, current_app
from sqlalchemy import select, update
from sqlalchemy.orm import Session

from v1.middlewares.utils import generate_uuid, check_unique
from v1.db.models import ShippingAddressTable, BalancesTable
from v1.db.bootstrap import get_engine, run_query
from v1.middlewares.utils import assert_in_dict, is_all_numbers
from v1.middlewares.requests import get_request, get_auth_token
from v1.middlewares.auth import authenticate_token
from v1.middlewares.middleware import auth_general

user_bp = Blueprint("user", __name__)

@user_bp.route("/user")
@auth_general()
def user_details():
    response = {}
    status = 401
    auth_info = authenticate_token(get_auth_token())
    response = {
        "data": {
            "name": auth_info.name,
            "email": auth_info.email,
            "phone_number": auth_info.phone_number
        }
    }
    status = 200
    return response, status

@user_bp.route("/user/shipping_address", methods=["POST"])
@auth_general()
def set_shipping_address():
    response = {}
    status = 400
    auth_info = authenticate_token(get_auth_token())
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "name", "phone_number", "address", "city")
    if not complete:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    else:
        if (not is_all_numbers(rqs["phone_number"])) or (len(rqs["phone_number"]) > 14) or (len(rqs["phone_number"]) < 10):
            response = {
                "error": "Invalid phone number format"
            }
        else:
            with Session(get_engine()) as session:
                is_unique, _ = check_unique("shipping_address", user_id=auth_info.id)
                if is_unique:
                    id = generate_uuid("shipping_address")
                    session.add(
                        ShippingAddressTable(
                            id=id, user_id=auth_info.id, name=rqs["name"],
                            phone_number=rqs["phone_number"], address=rqs["address"],
                            city=rqs["city"]
                        )
                    )
                else:
                    session.execute(
                        update(ShippingAddressTable)
                            .where(ShippingAddressTable.user_id == auth_info.id)
                            .values(
                                name=rqs["name"], phone_number=rqs["phone_number"],
                                address=rqs["address"], city=rqs["city"]
                            )
                    )
                session.commit()
            status = 200
            response = {
                "message": "Shipping address successfully set"
            }
    return response, status

@user_bp.route("/user/shipping_address")
@auth_general()
def get_shipping_address():
    response = {}
    status = 400
    auth_info = authenticate_token(get_auth_token())
    with Session(get_engine()) as session:
        addr_obj = session.scalar(select(ShippingAddressTable).where(ShippingAddressTable.user_id==auth_info.id))
    if not isinstance(addr_obj, ShippingAddressTable):
        status = 200
        response = {
            "message": "The specified user has no shipping address"
        }
    else:
        status = 200
        response = {
            "data": {
                "id": addr_obj.id,
                "name": addr_obj.name,
                "phone_number": addr_obj.phone_number,
                "address": addr_obj.address,
                "city": addr_obj.city
            }
        } 
    return response, status

@user_bp.route("/user/balance")
@auth_general()
def get_user_balance():
    response = {}
    status = 400
    auth_info = authenticate_token(get_auth_token())
    status = 200
    response = {
        "data": {
            "balance": auth_info.balance[0].balance
        }
    }
    return response, status

@user_bp.route("/user/balance", methods=["POST"])
@auth_general()
def add_user_balance():
    response = {}
    status = 400
    auth_info = authenticate_token(get_auth_token())
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "amount")
    if not complete:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    else:
        rqs["amount"] = str(rqs["amount"])
        if not is_all_numbers(rqs["amount"]):
            response = {
                "error": "Top-up amount must be a positive integer number"
            }
        else:
            with Session(get_engine()) as session:
                current_balance = session.scalar(select(BalancesTable).where(BalancesTable.user == auth_info))
                balance = int(current_balance.balance) + int(rqs["amount"])
                session.execute(
                    update(BalancesTable)
                        .where(BalancesTable.user == auth_info)
                        .values(balance=balance)
                )
                session.commit()
            status = 200
            response = {
                "message": "Top Up balance success"
            }
    return response, status