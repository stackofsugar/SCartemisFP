from uuid import uuid4
from flask import Blueprint, request
from sqlalchemy import select, update, delete
from sqlalchemy.orm import Session
from v1.middlewares.requests import get_auth_token

from v1.db.bootstrap import get_engine, run_query
from v1.db.models import ProductsTable, ProductImagesTable, ImagesTable, PersistentProductImagesTable, PersistentProductsTable
from v1.middlewares.requests import get_request
from v1.middlewares.utils import assert_in_dict, is_all_numbers, check_unique, generate_uuid, check_image, upload_image, get_images
from v1.middlewares.middleware import auth_admin

product_bp = Blueprint("products", __name__, url_prefix="/products")

# ADMIN
# - Create Product
@product_bp.route("/", methods=["POST"])
@auth_admin()
def add_product():
    rqs = get_request()
    status = 400
    response = {}
    complete, missing, empty = assert_in_dict(rqs, "product_name", "description", "condition", "category", "price", "images")
    rqs["price"] = str(rqs["price"])
    if complete == True:
        if is_all_numbers(rqs["price"]):
            is_new = False
            if rqs["condition"] == "new":
                is_new = True
            is_unique = True
            with Session(get_engine()) as session:
                not_unique_product = session.scalar(select(ProductsTable).filter(
                        ProductsTable.product_name == rqs["product_name"],
                        ProductsTable.category_id == rqs["category"],
                        ProductsTable.is_new == is_new
                ))
            if isinstance(not_unique_product, ProductsTable):
                is_unique = False
            with Session(get_engine()) as session:
                inactive_product = session.scalar(select(ProductsTable).filter(
                        ProductsTable.product_name == rqs["product_name"],
                        ProductsTable.category_id == rqs["category"],
                        ProductsTable.is_new == is_new,
                        ProductsTable.is_active == False,
                ))
            if isinstance(inactive_product, ProductsTable):
                with Session(get_engine()) as session:
                    session.execute(
                        update(ProductsTable)
                            .where(ProductsTable.id == inactive_product.id)
                            .values(is_active=True)
                    )
                    session.commit()
                status = 200
                response = {
                    "message": "Inactive product activated",
                }
            elif not is_unique:
                response = {
                    "error": "Product already exist"
                }
            else:
                is_unique_cat, not_unique_cat = check_unique("categories", id = rqs["category"])
                if not is_unique_cat:
                    images = list(rqs["images"])
                    images_have_errors = False
                    for image in images:
                        response, status = check_image(image)
                        if status != 200:
                            images_have_errors = True
                    if not images_have_errors:
                        product_id = generate_uuid("products")
                        images_ids = []
                        for image in images:
                            _, _, id = upload_image(image)
                            images_ids.append(id)
                        with Session(get_engine()) as session:
                            session.add(
                                ProductsTable(
                                    id=product_id,
                                    product_name = rqs["product_name"],
                                    description = rqs["description"],
                                    is_new = is_new,
                                    category_id = rqs["category"],
                                    price = rqs["price"],
                                    is_active = True
                                )
                            )
                            session.commit()
                            for image_id in images_ids:
                                session.add(
                                    ProductImagesTable(
                                        product_id=product_id,
                                        image_id=image_id
                                    )
                                )
                            session.commit()
                        status = 201
                        response = {
                            "message": "Product added",
                        }
                else:
                    response = {
                        "error": "Category does not exist",
                    }
        else:
            response = {
                "error": "Price should be a positive number"
            }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

# - Update Product
'''
TODO
- update image
- status code
'''
@product_bp.route("/", methods=["PUT"])
@auth_admin()
def update_product():
    rqs = get_request()
    status = 400
    response = {}
    complete, missing, empty = assert_in_dict(rqs, "product_name", "description", "condition", "category", "price", "product_id", "images")
    if complete:
        rqs["price"] = str(rqs["price"])
        with Session(get_engine()) as session:
            product_obj = session.scalar(select(ProductsTable).where(ProductsTable.id == rqs["product_id"]))
        if isinstance(product_obj, ProductsTable):
            if not product_obj.is_active:
                response = {
                    "error": "Product is deleted"
                }
            else:
                is_new = False
                if rqs["condition"] == "new":
                    is_new = True
                if not is_all_numbers(rqs["price"]):
                    response = {
                        "error": "Price should be all numbers"
                    }
                else:
                    image_ids = []
                    needadd_image = []
                    image_hasnoupdate = True
                    for image in rqs["images"]:
                        image_id = str(image).split("/image/", 1)
                        if len(image_id) > 1:
                            image_ids.append(image_id[1])
                        else:
                            _, status = check_image(image)
                            if status == 200:
                                needadd_image.append(image)
                    existing_images = get_images(product_obj, True)
                    needdelete_images = [x for x in existing_images if x not in image_ids]
                    if needadd_image or needdelete_images:
                        image_hasnoupdate = False
                    # return {
                    #     "existing_images": existing_images,
                    #     "needdelete_images": needdelete_images,
                    #     "needadd_image": needadd_image,
                    #     "image_ids": image_ids,
                    # }, 999
                    changes = [
                        product_obj.product_name == rqs["product_name"],
                        product_obj.description == rqs["description"],
                        product_obj.is_new == is_new,
                        product_obj.category_id == rqs["category"],
                        product_obj.price == int(rqs["price"]),
                        image_hasnoupdate,
                    ]
                    if all(changes):
                        status = 200
                        response = {
                            "message": "No changes were made"
                        }
                    else:
                        with Session(get_engine()) as session:
                            existing_product = session.scalar(
                                select(ProductsTable).filter(
                                    ProductsTable.product_name == rqs["product_name"],
                                    ProductsTable.category_id == rqs["category"],
                                    ProductsTable.is_new == is_new
                                )
                            )
                        have_duplicates_with_other_product = False
                        if isinstance(existing_product, ProductsTable):
                            if existing_product.id != rqs["product_id"]:
                                have_duplicates_with_other_product = True
                        if have_duplicates_with_other_product:
                            response = {
                                "error": "Product name, category, and condition should be unique"
                            }
                        else:
                            category_not_exist , _ = check_unique("categories", id = rqs["category"])
                            if category_not_exist:
                                response = {
                                    "error": "Category does not exist",
                                }
                            else:
                                image_haserrors = False
                                for image in needadd_image:
                                    response, status = check_image(image)
                                    if status != 200:
                                        image_haserrors = True
                                        break
                                if not image_haserrors:
                                    uploaded_image_ids = []
                                    for image in needadd_image:
                                        _, _, image_id = upload_image(image)
                                        uploaded_image_ids.append(image_id)
                                    with Session(get_engine()) as session:
                                        session.execute(update(ProductsTable)
                                            .where(ProductsTable.id == product_obj.id)
                                            .values(
                                                {
                                                    ProductsTable.product_name: rqs["product_name"],
                                                    ProductsTable.description: rqs["description"],
                                                    ProductsTable.is_new: is_new,
                                                    ProductsTable.category_id: rqs["category"],
                                                    ProductsTable.price: rqs["price"],
                                                }
                                            )
                                        )
                                        for image_id in uploaded_image_ids:
                                            session.add(
                                                ProductImagesTable(
                                                    product_id=product_obj.id,
                                                    image_id=image_id
                                                )
                                            )
                                        for image_id in needdelete_images:
                                            image_in_persistent = session.scalar(
                                                select(PersistentProductImagesTable)
                                                    .where(PersistentProductImagesTable.image_id == image_id)
                                            )
                                            session.execute(
                                                delete(ProductImagesTable)
                                                    .where(ProductImagesTable.image_id == image_id)
                                            )
                                            if not isinstance(image_in_persistent, PersistentProductImagesTable):
                                                session.execute(
                                                    delete(ImagesTable)
                                                        .where(ImagesTable.id == image_id)
                                                )
                                        session.commit()
                                    response = {
                                        "message": "Product updated"
                                    }
        else:
            response = {
                "error": "Invalid product ID"
            }
    else:
        response = {
                "error": f"Missing or empty parameters: {missing + empty}"
            }
    return response, status

# - Delete Product
@product_bp.route("/<product_id>", methods=["DELETE"])
@auth_admin()
def delete_product(product_id):
    status = 200
    is_unique, not_unique = check_unique("products", id = product_id)
    if not_unique:
        with Session(get_engine()) as session:
            session.execute(
                update(ProductsTable)
                    .filter(
                        ProductsTable.id == product_id, 
                        ProductsTable.is_active == 1
                    )
                    .values({ProductsTable.is_active: False})
                )
            session.commit()
        response = {
            "message": "Product deleted",
        }
    else:
        status = 400
        response = {
            "error": "Product does not exist"
        }
    return response, status

# ==Product List==
# - Get Product List
'''
TODO
- status code
- image url
'''
@product_bp.route("", methods=["GET"])
def product_list():
    args = get_request()
    response = {}
    status = 200
    complete, missing, empty = assert_in_dict(args, "sort_by", "category", "price", "condition", "product_name", "page_size", "page")
    result = []

    # Validate category
    categories = []
    if "category" not in (missing + empty):
        category = args.get("category")
        categories = str(category).split(",")
    # Validate condition
    if "condition" in missing:
        condition = [d['is_new'] for d in run_query(select(ProductsTable.is_new))]
    else:
        condition = args.get("condition")
        if condition == "used":
            condition = [False]
        elif condition == "new":
            condition = [True]
        else:
            response = {
                "error": "Condition should be used or new"
                }
            return response, status
    # Validate price
    if "price" not in missing:
        splitted = args.get("price").split(",")
        if len(splitted) == 2:
            range1 = splitted[0]
            range2 = splitted[1]
            if range1 >= range2 or is_all_numbers(range1) == False or is_all_numbers(range2) == False:
                response = {
                "error": "Price range is not valid"
                }
                return response, status
        else:
            response = {
            "error": "Price should be a range"
            }
            return response, status
    # Validate product_name
    if "product_name" in (empty + missing):
        product_name = None
    else:
        product_name = args["product_name"]
        if product_name in ["undefined", "null", "none"]:
            product_name = None

    try:
        page = int(args.get("page"))
    except:
        page = 1
    # Validate page_size
    if "page_size" in missing:
        page_size = 20
    else:
        if is_all_numbers(args.get("page_size")):
            page_size = int(args.get("page_size"))
            if page_size < 1:
                page_size = 5
        else:
            page_size = 5

    with Session(get_engine()) as session:
        if product_name:
            product_list = session.query(ProductsTable).filter(
                ProductsTable.product_name.like(f"%{product_name}%"),
                ProductsTable.is_new.in_(condition),
                ProductsTable.is_active == True,
            )
        else:
            product_list = session.query(ProductsTable).filter(
                ProductsTable.is_new.in_(condition),
                ProductsTable.is_active == True,
            )

    if "price" not in missing:
        with Session(get_engine()) as session:
            product_list = product_list.filter(ProductsTable.price.between(range1, range2))
    if categories:
        product_list = product_list.filter(ProductsTable.category_id.in_(categories))
    # Validate sort_by
    if "sort_by" in missing:
        product_list = product_list.order_by(ProductsTable.price.asc())
    else:
        if args.get("sort_by") == "Price a_z":
            product_list = product_list.order_by(ProductsTable.price.asc())
        elif args.get("sort_by") == "Price z_a":
            product_list = product_list.order_by(ProductsTable.price.desc())
    for obj in product_list:
        image_url = get_images(obj)
        if image_url:
            image_url = image_url[0]
        else:
            image_url = ""
        dictionary = {}
        dictionary["id"] = obj.id
        dictionary["image"] = image_url
        dictionary["title"] = obj.product_name
        dictionary["price"] = obj.price
        dictionary["category_id"] = obj.category_id
        result.append(dictionary)
    # List to list of lists
    paginated_list = [result[i: i+page_size] for i in range(0, len(result), page_size)]
    if len(paginated_list) == 0:
        paginated_list.append([])
    # Validate Page number
    if "page" in missing:
        page = 0
    else:
        if is_all_numbers(args.get("page")):
            if int(args.get("page")) > 0:
                if int(args.get("page")) <= len(paginated_list):
                    page = int(args.get("page"))-1
                else:
                    response = {
                        "error": "Page is out of range"
                    }
                    return response, status
            else:
                response = {
                    "error": "Page number should be positive number"
                }
                return response, status
        else:
            response = {
                "error": "Page number should be all numbers"
            }
            return response, status
    response = {
        "data": paginated_list[page],
        "total_rows": len(result)
    }
    return response, status

#USER
# ==Product Details Page==
# - Get Product Details
'''
TODO
- size
- image url
'''
@product_bp.route("/<product_id>", methods=["GET"])
def detail_product(product_id):
    status = 400
    with Session(get_engine()) as session:
        product_obj = session.scalar(select(ProductsTable).where(ProductsTable.id == product_id))
        if not isinstance(product_obj, ProductsTable):
            persistent_product_obj = session.scalar(
                select(PersistentProductsTable)
                    .where(PersistentProductsTable.id == product_id)
            )
            if not isinstance(persistent_product_obj, PersistentProductsTable):
                response = {
                    "error": "Product does not exist"
                }
            else:
                images_url = get_images(persistent_product_obj, persistent=True)
                status = 200
                response = {
                    "data": {
                        "id": persistent_product_obj.id,
                        "title": persistent_product_obj.product_name,
                        "size":  ["S", "M", "XL"],
                        "product_detail": persistent_product_obj.description,
                        "price": persistent_product_obj.price,
                        "images_url": images_url,
                        "category_id": persistent_product_obj.category_id,
                        "condition": "new" if persistent_product_obj.is_new else "used"
                    }
                }
        else:
            if not product_obj.is_active:
                response = {
                    "error": "Product is deleted"
                }
            else:
                images_url = get_images(product_obj)
                status = 200
                response = {
                        "data": {
                            "id": product_obj.id,
                            "title": product_obj.product_name,
                            "size":  ["S", "M", "XL"],
                            "product_detail": product_obj.description,
                            "price": product_obj.price,
                            "images_url": images_url,
                            "category_id": product_obj.category_id,
                            "condition": "new" if product_obj.is_new else "used"
                        }
                    }
    return response, status

# - Search by Image
@product_bp.route("/search_image", methods=["POST"])
def search_image():
    return {
        "error": "Feature not yet implemented"
    }, 501