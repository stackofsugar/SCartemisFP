import jwt
import bcrypt
import datetime
from uuid import uuid4
from flask import Blueprint
from sqlalchemy import select
from sqlalchemy.orm import Session
from email_validator import validate_email, EmailNotValidError

from v1.config import Config
from v1.db.bootstrap import get_engine
from v1.db.models import UsersTable, BalancesTable
from v1.middlewares.requests import get_request
from v1.middlewares.utils import assert_in_dict, is_all_numbers, has_lower, has_number, has_upper, check_unique, generate_uuid

auth_bp = Blueprint("auth", __name__)

@auth_bp.route("/sign-up", methods=["POST"])
def sign_up():
    rqs = get_request()
    status = 400
    response = {}
    complete, missing, empty = assert_in_dict(rqs, "name", "email", "phone_number", "password")

    if complete:
        # SECTION Validate Email
        valid_email = True
        try:
            valid = validate_email(rqs["email"])
            rqs["email"] = valid["email"]
        except EmailNotValidError as e:
            valid_email = False
            response = {
                "error": "Invalid Email address",
                "message": "Error! Invalid Email address",
            }
        # !SECTION Validate Email
        if valid_email:
        # SECTION Validate Phone number
            if not (is_all_numbers(rqs["phone_number"])):
                response = {
                    "error": "Phone number should be all numbers",
                    "message": "Error! Phone number should be all numbers",
                }
            else:
                if (len(rqs["phone_number"]) > 14) or (len(rqs["phone_number"]) < 10):
                    response = {
                        "error": "Phone number should be between 10 - 14 characters",
                        "message": "Error! Phone number should be between 10 - 14 characters",
                    }
        # !SECTION Validate Phone number
                else:
        # SECTION Validate password
                    has_password_error = True
                    if len(rqs["password"]) < 8:
                        response = {
                            "error": "Password must contain at least 8 characters",
                            "message": "Error! Password must contain at least 8 characters",
                        }
                    elif not has_lower(rqs["password"]):
                        response = {
                            "error": "Password must contain a lowercase letter",
                            "message": "Error! Password must contain a lowercase letter",
                        }
                    elif not has_upper(rqs["password"]):
                        response = {
                            "error": "Password must contain an uppercase letter",
                            "message": "Error! Password must contain an uppercase letter",
                        }
                    elif not has_number(rqs["password"]):
                        response = {
                            "error": "Password must contain a number",
                            "message": "Error! Password must contain a number",
                        }
                    else:
                        has_password_error = False
        # !SECTION Validate password
        # SECTION Validate unique
                    is_unique, not_unique = check_unique("users", email=rqs["email"], phone_number=rqs["phone_number"])
                    if not is_unique:
                        response = {
                            "error": f"Parameters should be unique: {not_unique}",
                            "message": f"Error! These fields should be unique: {not_unique}",
                        }
        # !SECTION Validate unique
                    else:
                        if not has_password_error:
                            password = bytes(rqs["password"], encoding="utf-8")
                            hashed = (bcrypt.hashpw(password, bcrypt.gensalt())).decode("utf-8")
                            id = generate_uuid("users")
                            with Session(get_engine()) as session:
                                session.add(
                                    UsersTable(
                                        id=id, name=rqs["name"], email=rqs["email"],
                                        phone_number=rqs["phone_number"], password=hashed, is_admin=False
                                    )
                                )
                                session.commit()
                                session.add(
                                    BalancesTable(
                                        user_id=id, balance=0
                                    )
                                )
                                session.commit()
                            status = 201
                            response = {
                                "message": "success, user created",
                            }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

@auth_bp.route("/sign-in", methods=["POST"])
def sign_in():
    rqs = get_request()
    status = 400
    response = {}
    complete, missing, empty = assert_in_dict(rqs, "email", "password")

    if complete:
        # SECTION Validate email
        with Session(get_engine()) as session:
            user_obj = session.scalar(select(UsersTable).where(UsersTable.email == rqs["email"]))
            if not isinstance(user_obj, UsersTable):
                response = {
                    "error": "Email and password does not match"
                }
        # !SECTION Validate email
            else:
        # SECTION Validate password
                hashed_bytes = bytes(user_obj.password, encoding="utf-8")
                pwd_bytes = bytes(rqs["password"], encoding="utf-8")
                if bcrypt.checkpw(pwd_bytes, hashed_bytes):
        # !SECTION Validate password
                    token = jwt.encode(
                        {
                            "uid": user_obj.id,
                            "iss": "fashion-campus-api",
                            "exp": datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(days=5)
                        },
                        Config.SECRET_KEY, algorithm="HS256"
                    )
                    status = 200
                    response = {
                        "message": "Login success",
                        "token": token,
                        "user_information": {
                            "name": user_obj.name,
                            "email": user_obj.email,
                            "phone_number": user_obj.phone_number,
                            "type": "seller" if user_obj.is_admin else "buyer",
                        }
                    }
                else:
                    response = {
                        "error": "Email and password does not match"
                    }

    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status