from flask import Blueprint, request, current_app
from sqlalchemy import select
from sqlalchemy.orm import Session

from v1.db.models import TestTable
from v1.db.bootstrap import get_engine, run_query
from v1.middlewares.requests import get_request, get_headers, get_auth_token, get_files
from v1.middlewares.auth import authenticate_token
from v1.middlewares.middleware import auth_general, auth_admin
from v1.middlewares.utils import upload_image

test_bp = Blueprint("test", __name__, url_prefix="/test")

@test_bp.route('/')
def test_index():
    return {
        "message": "We are testing!",
    }

@test_bp.route('/environ')
def test_environ():
    envs = {}
    for key in current_app.config:
        envs[key] = str(current_app.config[key])
    return {
        "message": "<hidden>"
    }

@test_bp.route('/sql')
def test_sql():
    message = "Everything is OK"
    with Session(get_engine()) as session:
        run_query(f"TRUNCATE TABLE {TestTable.__tablename__}", True)
        names = ["Jamal", "Chris", "Bob", "Johnson"]
        for name in names:
            session.add(
                TestTable(name=name)
            )
        session.commit()
        session.delete(session.scalar(select(TestTable).where(TestTable.name == "Bob")))
        session.commit()

        asserter = {}
        res1 = session.scalars(select(TestTable))
        for row in res1:
            asserter[row.id] = row.name
            
        expected = {
            1: "Jamal",
            2: "Chris",
            4: "Johnson"
        }

        if asserter != expected:
            message = {
                "error": "Test result failed assertion",
                "yours": asserter,
                "expected": expected,
            }

    return {
        "message": message,
    }

@test_bp.route("/request", methods=["POST"])
def test_request():
    return {
        "auth": get_auth_token(),
        "request_all": get_request(),
        "header_all": get_headers(),
        "header Postman-Token": get_headers("Postman-Token")
    }

@test_bp.route("/auth-token", methods=["POST"])
def auth_token():
    auth_stat = authenticate_token(get_auth_token())
    return {
        "payload": {
            "id": auth_stat.id,
            "name": auth_stat.name,
            "email": auth_stat.email,
            "phone_number": auth_stat.phone_number,
            "type": "seller" if auth_stat.is_admin else "buyer"
        }
    }

@test_bp.route("/middleware/auth-only")
@auth_general()
def test_middleware1():
    response = {
        "message": "Hello!"
    }
    return response, 200

@test_bp.route("/middleware/admin-only")
@auth_admin()
def test_middleware2():
    response = {
        "message": "Hello!"
    }
    return response, 200

@test_bp.route("/fileupload", methods=["POST"])
def file_upload():
    response = {
        "payload": get_files()
    }
    return response, 200

@test_bp.route("/base64image_upload", methods=["POST"])
def base64image_upload():
    rqs = get_request()
    if "file" in rqs.keys():
        response, status, image_id = upload_image(rqs["file"])
        if status == 201:
            response = {
                "message": f"Image ID: {image_id}"
            }
        return response, status
    else:
        return "hah mana"