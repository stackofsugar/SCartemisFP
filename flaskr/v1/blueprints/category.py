from flask import Blueprint, request, current_app
from sqlalchemy import select, update
from sqlalchemy.orm import Session
from sqlalchemy.engine.result import ScalarResult

from v1.db.models import CategoriesTable, ProductsTable, ProductImagesTable
from v1.middlewares.utils import generate_uuid, get_images
from v1.db.bootstrap import get_engine, run_query
from v1.middlewares.utils import assert_in_dict, is_all_numbers
from v1.middlewares.requests import get_request, get_auth_token
from v1.middlewares.auth import authenticate_token
from v1.middlewares.middleware import auth_admin

category_bp = Blueprint("category", __name__)

@category_bp.route("/categories", methods=["POST"])
@auth_admin()
def create_category():
    response = {}
    status = 400
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "category_name")
    if not complete:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    else:
        with Session(get_engine()) as session:
            existing_category = session.scalar(select(CategoriesTable).where(CategoriesTable.name == rqs["category_name"]))
            if isinstance(existing_category, CategoriesTable):
                if existing_category.active == False:
                    session.execute(
                        update(CategoriesTable)
                            .where(CategoriesTable.id == existing_category.id)
                            .values(active=True)
                    )
                    status = 200
                    response = {
                        "message": "Deleted category restored"
                    }
                else:
                    response = {
                        "error": "Category name already exists"
                    }
            else:
                id = generate_uuid("categories")
                session.add(
                    CategoriesTable(
                        id=id, name=rqs["category_name"], active=True
                    )
                )
                status = 201
                response = {
                    "message": "Category added"
                }
            session.commit()
    return response, status

@category_bp.route("/categories/<id>", methods=["PUT"])
@auth_admin()
def update_category(id = None):
    response = {}
    status = 400
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "category_name")
    if not id:
        response = {
            "error": "ID not supplied. Correct usage: /categories/<ID>"
        }
    elif not complete:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    else:
        with Session(get_engine()) as session:
            old_value = session.scalar(select(CategoriesTable).where(CategoriesTable.id == id))
            if not old_value.active:
                response = {
                    "error": "Category has been deleted"
                }
            elif old_value.name != rqs["category_name"]:
                session.execute(
                    update(CategoriesTable)
                        .where(CategoriesTable.id == id)
                        .values(name=rqs["category_name"])
                )
                session.commit()
                status = 200
                response = {
                    "message": "Category updated"
                }
            else:
                status = 200
                response = {
                    "message": "Same new value, category unchanged"
                }
    return response, status

@category_bp.route("/categories/<id>", methods=["DELETE"])
@auth_admin()
def delete_category(id = None):
    response = {}
    status = 400
    if not id:
        response = {
            "error": "ID not supplied. Correct usage: /categories/<ID>"
        }
    else:
        with Session(get_engine()) as session:
            category_object = session.scalar(select(CategoriesTable).where(CategoriesTable.id == id))
            if not category_object:
                response = {
                    "error": "No existing category corresponds to given ID"    
                }
            elif category_object.active == False:
                response = {
                    "error": "Category is already deleted"
                }
            else:
                session.execute(
                    update(CategoriesTable)
                        .where(CategoriesTable.id == id)
                        .values(active=False)
                )
                session.commit()
                status = 200
                response = {
                    "message": "Category deleted"
                }
    return response, status

@category_bp.route("/home/category", methods=["GET"])
def get_all_category():
    response = {}
    status = 200
    with Session(get_engine()) as session:
        all_categories = session.scalars(select(CategoriesTable).where(CategoriesTable.active == True))
        returned_result = []
        if isinstance(all_categories, ScalarResult):
            for item in all_categories.unique().all():
                if isinstance(item, CategoriesTable):
                    category_image_link = ""
                    products_of_category = session.scalars(
                        select(ProductsTable)
                            .where(ProductsTable.category_id == item.id)
                            .order_by(ProductsTable.price.desc())
                    )
                    if isinstance(products_of_category, ScalarResult):
                        for product in products_of_category.unique().all():
                            images = get_images(product)
                            if images:
                                category_image_link = images[0]
                                break
                    returned_result.append({
                        "id": item.id,
                        "title": item.name,
                        "image": category_image_link,
                    })
        response = {
            "data": returned_result
        }
    return response, status

@category_bp.route("/categories", methods=["GET"])
def get_product_categories():
    response = {}
    status = 200
    with Session(get_engine()) as session:
        all_categories = session.scalars(select(CategoriesTable).where(CategoriesTable.active == True))
        returned_result = []
        if isinstance(all_categories, ScalarResult):
            for item in all_categories.unique().all():
                if isinstance(item, CategoriesTable):
                    returned_result.append(
                        {
                            "id": item.id,
                            "title": item.name,
                        }
                    )
        response = {
            "data": returned_result
        }
    return response, status