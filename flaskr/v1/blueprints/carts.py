from flask import Blueprint
from sqlalchemy import select, update, delete
from sqlalchemy.orm import Session
from sqlalchemy.engine.result import ScalarResult
from datetime import datetime

from v1.middlewares.requests import get_auth_token
from v1.db.bootstrap import get_engine, run_query
from v1.db.models import ProductsTable, CartsTable, OrdersTable, OrderProductsTable, PersistentProductsTable, BalancesTable, PersistentProductImagesTable
from v1.middlewares.requests import get_request
from v1.middlewares.utils import assert_in_dict, is_all_numbers, generate_uuid, get_images, calculate_shipping_price
from v1.middlewares.middleware import auth_admin, auth_general
from v1.middlewares.auth import authenticate_token

carts_bp = Blueprint("carts", __name__)

@carts_bp.route("/cart", methods=["POST"])
@auth_general()
def add_to_cart():
    response = {}
    status = 400
    rqs = get_request()
    user_obj = authenticate_token(get_auth_token())
    complete, missing, empty = assert_in_dict(rqs, "id", "quantity", "size")
    if complete:
        with Session(get_engine()) as session:
            item_obj = session.scalar(
                select(ProductsTable).where(ProductsTable.id == rqs["id"])
            )
        if not isinstance(item_obj, ProductsTable):
            response = {
                "error": "Product does not exist",
                "message": "Error! Product does not exist",
            }
        elif (not is_all_numbers(rqs["quantity"])) or (int(rqs["quantity"]) <= 0):
            response = {
                "error": "Quantity should be a positive number"
            }
        else:
            size = "S" 
            if rqs["size"] == "M": size = "M"
            elif rqs["size"] == "XL": size = "XL"
            with Session(get_engine()) as session:
                existing_product_cart_obj = session.scalar(
                    select(CartsTable).filter(
                        CartsTable.user_id == user_obj.id,
                        CartsTable.item_id == item_obj.id,
                        CartsTable.size == size
                    )
                )
            if isinstance(existing_product_cart_obj, CartsTable):
                new_qty = int(existing_product_cart_obj.qty) + int(rqs["quantity"])
                with Session(get_engine()) as session:
                    session.execute(
                        update(CartsTable)
                            .where(CartsTable.id == existing_product_cart_obj.id)
                            .values(qty=new_qty)
                    )
                    session.commit()
            else:
                id = generate_uuid("carts")
                with Session(get_engine()) as session:
                    session.add(
                        CartsTable(
                            id=id,
                            user_id=user_obj.id,
                            item_id=item_obj.id,
                            qty=rqs["quantity"],
                            size=size
                        )
                    )
                    session.commit()
            status = 200
            response = {
                "message": "success! Item added to cart"
            }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

@carts_bp.route("/cart", methods=["GET"])
@auth_general()
def get_cart():
    response = {}
    status = 400
    user_obj = authenticate_token(get_auth_token())
    carts = []
    cart_return_data = []
    with Session(get_engine()) as session:
        carts_scalar = session.scalars(
            select(CartsTable)
                .where(CartsTable.user_id == user_obj.id)
        )
        if isinstance(carts_scalar, ScalarResult):
            carts = carts_scalar.unique().all()
    for cart_item in carts:
        if isinstance(cart_item, CartsTable):
            image = get_images(cart_item.products)
            if image:
                image = image[0]
            else:
                image = ""
            cart_return_data.append({
                "id": cart_item.id,
                "details": {
                    "quantity": cart_item.qty,
                    "size": cart_item.size
                },
                "price": cart_item.products.price,
                "image": image,
                "name": cart_item.products.product_name
            })
    status = 200
    response = {
        "data": cart_return_data
    }
    return response, status

@carts_bp.route("/cart/<cart_id>", methods=["DELETE"])
@auth_general()
def delete_cart(cart_id):
    response = {}
    status = 400
    user_obj = authenticate_token(get_auth_token())
    with Session(get_engine()) as session:
        cart_item = session.scalar(
            select(CartsTable)
                .where(CartsTable.id == cart_id)
        )
    if isinstance(cart_item, CartsTable):
        if cart_item.user_id != user_obj.id:
            response = {
                "error": "Cart does not belong to the user"
            }
        else:
            with Session(get_engine()) as session:
                session.execute(
                    delete(CartsTable)
                        .where(CartsTable.id == cart_id)
                )
                session.commit()
            status = 200
            response = {
                "message": "Cart deleted"
            }
    else:
        response = {
            "error": "Cart does not exist"
        }
    return response, status

@carts_bp.route("/shipping_price", methods=["GET"])
@auth_general()
def get_shipping_price():
    response = {}
    status = 400
    user_obj = authenticate_token(get_auth_token())
    carts = []
    total_price = int()
    with Session(get_engine()) as session:
        carts_scalar = session.scalars(
            select(CartsTable)
                .where(CartsTable.user_id == user_obj.id)
        )
        if isinstance(carts_scalar, ScalarResult):
            carts = carts_scalar.unique().all()
    if len(carts) <= 0:
        response = {
            "error": "Your cart is empty"
        }
    else:
        for cart_item in carts:
            if isinstance(cart_item, CartsTable):
                total_price += (int(cart_item.products.price) * int(cart_item.qty))
        status = 200
        response = {
            "data": calculate_shipping_price(total_price)
        }
    return response, status

@carts_bp.route("/order", methods=["POST"])
@auth_general()
def create_order():
    response = {}
    status = 400
    rqs = get_request()
    user_obj = authenticate_token(get_auth_token())
    complete, missing, empty = assert_in_dict(rqs, "shipping_method", "shipping_address")
    if complete:
        if rqs["shipping_method"] not in ["regular", "next day"]:
            response = {
                "error": "Shipping method should be either 'regular' or 'next day'"
            }
        else:
            shipping_is_nextday = True if rqs["shipping_method"] == "next day" else False
            total_price = int()
            products_price = int()
            products = []
            products_dict = []
            with Session(get_engine()) as session:
                carts_scalar = session.scalars(
                    select(CartsTable)
                        .where(CartsTable.user_id == user_obj.id)
                )
                if isinstance(carts_scalar, ScalarResult):
                    carts = carts_scalar.unique().all()
            if len(carts) <= 0:
                response = {
                    "error": "Your cart is empty"
                }
            else:
                for cart_item in carts:
                    if isinstance(cart_item, CartsTable):
                        products_price += (int(cart_item.products.price) * int(cart_item.qty))
                        products.append(cart_item.products)
                        products_dict.append({
                            "product_id": cart_item.products,
                            "product": cart_item.products,
                            "qty": cart_item.qty,
                            "size": cart_item.size,
                        })
                complete, missing, empty = assert_in_dict(rqs["shipping_address"], "name", "phone_number", "address", "city")
                if complete:
                    if (not is_all_numbers(rqs["shipping_address"]["phone_number"])) or (len(rqs["shipping_address"]["phone_number"]) not in range(10, 14)):
                        response = {
                            "error": "Invalid phone number format"
                        }
                    else:
                        shipping_price = int()
                        shipping_prices = calculate_shipping_price(products_price)
                        for item in shipping_prices:
                            if item["name"] == rqs["shipping_method"]:
                                shipping_price = item["price"]
                        total_price = products_price + shipping_price
                        # response = {
                        #     "products_price": products_price,
                        #     "shipping_price": shipping_price,
                        #     "total_price": total_price,
                        #     "rqs['shipping_address']": rqs['shipping_address'],
                        #     "shipping_is_nextday": shipping_is_nextday,
                        #     "now": datetime.now()
                        # }
                        user_balance = user_obj.balance[0].balance
                        if total_price > user_balance:
                            response = {
                                "error": "insufficient funds",
                                "data": {
                                    "your_balance": user_balance,
                                    "total_price": total_price,
                                }
                            }
                        else:
                            order_id = generate_uuid("orders")
                            with Session(get_engine()) as session:
                                session.add(
                                    OrdersTable(
                                        id=order_id,
                                        user_id=user_obj.id,
                                        is_nextday=shipping_is_nextday,
                                        created_at=datetime.now(),
                                        sa_name=rqs["shipping_address"]["name"],
                                        sa_city=rqs["shipping_address"]["city"],
                                        sa_address=rqs["shipping_address"]["address"],
                                        sa_phone_number=rqs["shipping_address"]["phone_number"],
                                    )
                                )
                                session.execute(
                                    update(BalancesTable)
                                        .where(BalancesTable.user_id == user_obj.id)
                                        .values(balance=(user_balance - total_price))
                                )
                                for product in products_dict:
                                    if isinstance(product["product"], ProductsTable):
                                        product_image_ids = get_images(product["product"], id_only=True)
                                        existing_persistent_product_obj = session.scalar(
                                            select(PersistentProductsTable)
                                                .filter(
                                                    PersistentProductsTable.category_id == product["product"].category_id,
                                                    PersistentProductsTable.product_name == product["product"].product_name,
                                                    PersistentProductsTable.description == product["product"].description,
                                                    PersistentProductsTable.is_new == product["product"].is_new,
                                                    PersistentProductsTable.price == product["product"].price,
                                                )
                                        )
                                        have_images_difference = False
                                        if isinstance(existing_persistent_product_obj, PersistentProductsTable):
                                            for image_id in product_image_ids:
                                                existing_persistent_product_image_obj = session.scalar(
                                                    select(PersistentProductImagesTable)
                                                        .filter(
                                                            PersistentProductImagesTable.product_id == existing_persistent_product_obj.id,
                                                            PersistentProductImagesTable.image_id == image_id,
                                                        )
                                                )
                                                if not isinstance(existing_persistent_product_image_obj, PersistentProductImagesTable):
                                                    have_images_difference = True
                                                    break
                                        if isinstance(existing_persistent_product_obj, PersistentProductsTable) and not have_images_difference:
                                            product["product_id"] = existing_persistent_product_obj.id
                                        else:
                                            persistent_product_id = generate_uuid("persistent_products")
                                            product["product_id"] = persistent_product_id
                                            session.add(
                                                PersistentProductsTable(
                                                    id=persistent_product_id,
                                                    category_id=product["product"].category_id,
                                                    product_name=product["product"].product_name,
                                                    description=product["product"].description,
                                                    is_new=product["product"].is_new,
                                                    price=product["product"].price,
                                                )
                                            )
                                            session.commit()
                                            for product_image_id in product_image_ids:
                                                session.add(
                                                    PersistentProductImagesTable(
                                                        product_id=persistent_product_id,
                                                        image_id=product_image_id
                                                    )
                                                )
                                session.commit()
                                for product in products_dict:
                                    session.add(
                                        OrderProductsTable(
                                            persistent_product_id=product["product_id"],
                                            order_id=order_id,
                                            qty=product["qty"],
                                            size=product["size"]
                                        )
                                    )
                                session.execute(
                                    delete(CartsTable)
                                        .where(CartsTable.user_id == user_obj.id)
                                )
                                session.commit()
                            status = 201
                            response = {
                                "message": "Order success"
                            }
                else:
                    response = {
                        "error": f"Missing or empty parameters from 'shipping_address': {missing + empty}"
                    }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

@carts_bp.route("/user/order", methods=["GET"])
@auth_general()
def get_user_orders():
    response = {}
    status = 200
    user_obj = authenticate_token(get_auth_token())
    user_orders = []
    returned_user_orders = []
    with Session(get_engine()) as session:
        user_orders_scalar = session.scalars(
            select(OrdersTable)
                .where(OrdersTable.user_id == user_obj.id)
                .order_by(OrdersTable.created_at.desc())
        )
        if isinstance(user_orders_scalar, ScalarResult):
            user_orders = user_orders_scalar.unique().all()
    for user_order in user_orders:
        if isinstance(user_order, OrdersTable):
            returned_order_products = []
            persistent_products_joined = []
            order_products = []
            with Session(get_engine()) as session:
                order_products_scalar = session.scalars(
                    select(OrderProductsTable)
                        .where(OrderProductsTable.order_id == user_order.id)
                )
                if isinstance(order_products_scalar, ScalarResult):
                    order_products = order_products_scalar.unique().all()
                for order_product in order_products:
                    if isinstance(order_product, OrderProductsTable):
                        persistent_products_joined.append({
                            "qty": order_product.qty,
                            "size": order_product.size,
                            "product_id": order_product.persistent_product_id,
                        })
                for pers_prod_joined in persistent_products_joined:
                    persistent_product = session.scalar(
                        select(PersistentProductsTable)
                            .where(PersistentProductsTable.id == pers_prod_joined["product_id"])
                    )
                    if isinstance(persistent_product, PersistentProductsTable):
                        image_url = get_images(persistent_product, persistent=True)
                        if image_url:
                            image_url = image_url[0]
                        else:
                            image_url = ""
                        returned_order_products.append({
                            "id": persistent_product.id,
                            "details": {
                                "quantity": pers_prod_joined["qty"],
                                "size": pers_prod_joined["size"],
                            },
                            "price": persistent_product.price,
                            "image": image_url,
                            "name": persistent_product.product_name
                        })
            returned_user_orders.append({
                "id": user_order.id,
                "created_at": user_order.created_at,
                "shipping_method": "next day" if user_order.is_nextday else "regular",
                "status": user_order.status,
                "shipping_address": {
                    "name": user_order.sa_name,
                    "phone_number": user_order.sa_phone_number,
                    "address": user_order.sa_address,
                    "city": user_order.sa_city,
                },
                "products": returned_order_products,
            })
    response = {
        "data": returned_user_orders
    }
    return response, status

def price_sort_key(e):
    return e["total"]

@carts_bp.route("/orders", methods=["GET"])
@auth_admin()
def get_all_orders():
    data = []
    response = {}
    status = 200
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "sort_by", "page", "page_size")
    all_orders = []
    returned_orders_raw = []
    price_sort_asc = False
    page_size = 5
    cur_page = 1
    if "sort_by" not in (missing + empty):
        if rqs["sort_by"] == "Price a_z":
            price_sort_asc = True
    if "page_size" not in (missing + empty):
        if is_all_numbers(str(rqs["page_size"])):
            page_size = int(rqs["page_size"])
    if "page" not in (missing + empty):
        if is_all_numbers(str(rqs["page"])):
            cur_page = int(rqs["page"])
            if cur_page < 1:
                cur_page = 1
    with Session(get_engine()) as session:
        all_orders_scalar = session.scalars(select(OrdersTable))
        if isinstance(all_orders_scalar, ScalarResult):
            all_orders = all_orders_scalar.unique().all()
        for order in all_orders:
            total_price = 0
            if isinstance(order, OrdersTable):
                order_prods = session.scalars(
                    select(OrderProductsTable)
                        .where(OrderProductsTable.order_id == order.id)
                )
                if isinstance(order_prods, ScalarResult):
                    order_prods = order_prods.unique().all()
                for order_prod in order_prods:
                    if isinstance(order_prod, OrderProductsTable):
                        prod_obj = session.scalar(
                            select(PersistentProductsTable)
                                .where(PersistentProductsTable.id == order_prod.persistent_product_id)
                        )
                        if isinstance(prod_obj, PersistentProductsTable):
                            total_price += int(prod_obj.price * order_prod.qty)
                returned_orders_raw.append({
                    "id": order.id,
                    "user_email": order.user.email,
                    "user_name": order.user.name,
                    "created_at": order.created_at,
                    "user_id": order.user.id,
                    "total": total_price,
                })                
    if price_sort_asc:
        returned_orders_raw.sort(key=price_sort_key)
    else:
        returned_orders_raw.sort(key=price_sort_key, reverse=True)
    start_page = page_size * (cur_page - 1)
    for i in range(start_page, start_page + page_size):
        if i < len(returned_orders_raw):
            data.append(returned_orders_raw[i])
    response = {
        "data": data
    }
    return response, status

@carts_bp.route("/sales", methods=["GET"])
@auth_admin()
def get_total_sales():
    response = {}
    status = 200
    order_products = []
    order_products_joined = []
    total_revenue = int()
    with Session(get_engine()) as session:
        order_products_scalar = session.scalars(select(OrderProductsTable))
        if isinstance(order_products_scalar, ScalarResult):
            order_products = order_products_scalar.unique().all()
    for order_product in order_products:
        if isinstance(order_product, OrderProductsTable):
            order_products_joined.append({
                "product_id": order_product.persistent_product_id,
                "qty": order_product.qty,
                "price": 0,
            })
    for or_prod_join in order_products_joined:
        product = None
        with Session(get_engine()) as session:
            product = session.scalar(
                select(PersistentProductsTable)
                    .where(PersistentProductsTable.id == or_prod_join["product_id"])
            )
        if isinstance(product, PersistentProductsTable):
            or_prod_join["price"] = int(product.price)
    for or_prod_join in order_products_joined:
        total_revenue += (int(or_prod_join["price"]) * int(or_prod_join["qty"]))
    response = {
        "data": {
            "total": total_revenue
        }
    }
    return response, status