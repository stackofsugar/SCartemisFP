from io import BytesIO
import base64
from flask import Blueprint, send_from_directory, abort, send_file
from sqlalchemy import select, delete
from sqlalchemy.orm import Session
from sqlalchemy.engine.result import ScalarResult

from v1.middlewares.utils import assert_in_dict, generate_uuid, check_image
from v1.middlewares.requests import get_request
from v1.middlewares.middleware import auth_admin
from v1.config import Config
from v1.db.bootstrap import get_engine
from v1.db.models import ImagesTable, BannersTable

public_bp = Blueprint("public_resources", __name__)

@public_bp.route("/")
def landing():
    return {
        "message": "Welcome to Fashion Campus API - Artemis Team",
        "service-health": "healthy",
    }, 200

@public_bp.route("/imagefile/<filename>")
def serve_image(filename=None):
    status = 400
    response = {}
    if not filename:
        response = {
            "error": "No filename specified. Usage: /image/<filename>"
        }
    else:
        status = 200
        return send_from_directory(
            Config.UPLOAD_FOLDER, filename, as_attachment=True
        )
    return response, status

@public_bp.route("/image/<filename>")
def serve_base64_image(filename=None):
    status = 400
    response = {}
    if not filename:
        response = {
            "error": "No filename specified. Usage: /image/<filename>"
        }
    else:
        filename = str(filename)
        filename_without_ext = filename.rsplit(".", 1)[0]
        with Session(get_engine()) as session:
            item = session.scalar(select(ImagesTable).where(ImagesTable.id == filename_without_ext))
        if isinstance(item, ImagesTable):
            status = 200
            mimetype = str(item.base64image).split(";", 2)[0].split(":", 2)[1]
            fileext = str(mimetype).split("/", 2)[1]
            base64bytes = str(item.base64image).split(",", 2)[1]
            image_stream = BytesIO(base64.decodebytes(bytes(base64bytes, encoding="utf-8")))
            return send_file(
                image_stream, mimetype, as_attachment=True, download_name=filename+"."+fileext
            )
        else:
            status = 404
            response = {
                "error": "Image not found"
            }
    return response, status

@public_bp.route("/home/banner", methods=["POST"])
@auth_admin()
def add_banner():
    status = 400
    response = {}
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "image", "title")
    if complete:
        response, status = check_image(rqs["image"])
        if status == 200:
            with Session(get_engine()) as session:
                id = generate_uuid("banners")
                session.add(
                    BannersTable(
                        id=id,
                        base64image=rqs["image"],
                        title=rqs["title"]
                    )
                )
                session.commit()
            status = 201
            response = {
                "message": "Banner created"
            }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

@public_bp.route("/home/banner", methods=["PUT"])
@auth_admin()
def edit_banner():
    status = 400
    response = {}
    rqs = get_request()
    complete, missing, empty = assert_in_dict(rqs, "id", "image", "title")
    if complete:
        with Session(get_engine()) as session:
            existing_banner = session.scalar(
                select(BannersTable)
                    .where(BannersTable.id == rqs["id"])
            )
            if isinstance(existing_banner, BannersTable):
                nothing_to_update = [
                    existing_banner.base64image == rqs["image"],
                    existing_banner.title == rqs["title"],
                ]
                if all(nothing_to_update):
                    status = 200
                    response = {
                        "message": "Nothing to update"
                    }
                else:
                    existing_banner.base64image = rqs["image"]
                    existing_banner.title = rqs["title"]
                    session.commit()
                    status = 200
                    response = {
                        "message": "Banner updated"
                    }
            else:
                response = {
                    "error": "Banner do not exist"
                }
    else:
        response = {
            "error": f"Missing or empty parameters: {missing + empty}"
        }
    return response, status

@public_bp.route("/home/banner/<id>", methods=["DELETE"])
@auth_admin()
def delete_banner(id):
    status = 400
    response = {}
    with Session(get_engine()) as session:
        banner_obj = session.scalar(
            select(BannersTable)
                .where(BannersTable.id == id)
        )
        if isinstance(banner_obj, BannersTable):
            session.execute(
                delete(BannersTable)
                    .where(BannersTable.id == id)
            )
            status = 200
            response = {
                "message": "Banner deleted successfully"
            }
        else:
            response = {
                "error": "Specified banner not found"
            }
        session.commit()
    return response, status

@public_bp.route("/home/banner/")
def get_banner():
    status = 200
    response = {}
    banners = []
    banners_list = []
    with Session(get_engine()) as session:
        banners_scalars = session.scalars(
            select(BannersTable)
        )
        if isinstance(banners_scalars, ScalarResult):
            banners_list = banners_scalars.unique().all()
        for banner in banners_list:
            if isinstance(banner, BannersTable):
                banners.append({
                    "id": banner.id,
                    "image": banner.base64image,
                    "title": banner.title,
                })
    response = {
        "data": banners
    }
    return response, status