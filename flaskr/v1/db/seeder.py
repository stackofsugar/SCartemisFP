from sqlalchemy import select, update
from sqlalchemy.orm import Session
from uuid import uuid4
import bcrypt

from bootstrap import get_engine, run_query

def check_unique(tablename: str, **kwargs):
    """Check database uniqueness"""
    not_unique = []
    for key, value in kwargs.items():
        result = run_query(f"""
            SELECT * FROM {tablename}
            WHERE {key} = '{value}'
        """)
        if result:
            not_unique.append(key)
    if not not_unique:
        return True, []
    else:
        return False, not_unique

def generate_uuid(tablename: str):
    while True:
        uuid = str(uuid4())
        is_unique, _ = check_unique(tablename, id=uuid)
        if is_unique:
            return uuid

def insertOrIgnore(tablename: str, data: dict) -> bool:
    for key in data:
        if key != "id":
            result = run_query(f"""
                SELECT * FROM {tablename}
                WHERE {key} = '{data[key]}'
            """)
            if result:
                return False
    columns = ""
    query = f"""
        INSERT INTO {tablename}
    """
    

if __name__ == "__main__":
    password = "someone_neW5"
    password = bytes(password, encoding="utf-8")
    hashed = (bcrypt.hashpw(password, bcrypt.gensalt())).decode("utf-8")
    userstable = "users"
    users = [
        [generate_uuid(userstable), "Abiyyu Sasangka", "abisang@myorganization.com", "09871233256", hashed],
        [generate_uuid(userstable), "John Smith", "jsmith@myorganization.com", "068764153782", hashed],
        [generate_uuid(userstable), "Christopher Benny", "cbenny@myorganization.com", "083579841236", hashed],
    ]
    users_mapped = []
    for user in users:
        insertOrIgnore("users", {
            "id": user[0],
            "name": user[1],
            "email": user[2],
            "phone_number": user[3],
            "password": user[4],
            "is_admin": False,
        })