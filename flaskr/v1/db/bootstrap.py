from dotenv import dotenv_values
from sqlalchemy import create_engine, text

config = dotenv_values()

creds = {
    "user": config["DB_USERNAME"],
    "pass": config["DB_PASSWORD"],
    "host": config["DB_HOST"],
    "port": config["DB_PORT"],
    "db": config["DB_DATABASE"],
}

dbstr = "mariadb+pymysql://" + creds["user"] + ":" + creds["pass"] + "@" + creds["host"] + ":" + creds["port"] + "/" + creds["db"] + "?charset=utf8mb4"

def get_engine():
    return create_engine(dbstr, echo=True, future=True)

def run_query(query, commit: bool = False):
    engine = get_engine()
    if isinstance(query, str):
        query = text(query)

    with engine.connect() as conn:
        if commit:
            conn.execute(query)
            conn.commit()
        else:
            return [dict(row) for row in conn.execute(query)]
