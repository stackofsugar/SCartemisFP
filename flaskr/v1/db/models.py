from sqlalchemy import Column, Integer, String, Boolean, BigInteger, ForeignKey, Text, UniqueConstraint, DateTime, SmallInteger
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.dialects.mysql import TEXT, MEDIUMTEXT

from v1.db.bootstrap import get_engine

Base = declarative_base()
engine = get_engine()

class TestTable(Base):
    __tablename__ = "test_table"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))

    def __repr__(self) -> str:
        return f"TestTable(id={self.id}), name='{self.name}'"

class UsersTable(Base):
    __tablename__ = "users"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    name = Column(String(255), nullable=False)
    email = Column(String(255), unique=True, nullable=False)
    phone_number = Column(String(14), unique=True, nullable=False)
    password = Column(String(255, collation="utf8mb4_bin"), nullable=False)
    is_admin = Column(Boolean(), nullable=False)

    balance = relationship("BalancesTable", back_populates="user", lazy="joined")
    shipping_address = relationship("ShippingAddressTable", back_populates="user", lazy="joined")
    cart = relationship("CartsTable", back_populates="user", lazy="joined")
    orders = relationship("OrdersTable", back_populates="user", lazy="joined")

    def __repr__(self) -> str:
        pass

class OrdersTable(Base):
    __tablename__ = "orders"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    user_id = Column(String(36), ForeignKey("users.id"))
    sa_name = Column(String(255), nullable=False)
    sa_city = Column(String(255), nullable=False)
    sa_address = Column(TEXT(), nullable=False)
    sa_phone_number = Column(String(14), nullable=False)
    is_nextday = Column(Boolean(), nullable=False)
    created_at = Column(DateTime(), nullable=False)
    status = Column(String(255), default="waiting")

    user = relationship("UsersTable", back_populates="orders", lazy="joined")

class OrderProductsTable(Base):
    __tablename__ = "order_products"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(BigInteger(), primary_key=True, autoincrement=True)
    persistent_product_id = Column(String(36), ForeignKey("persistent_products.id"))
    order_id = Column(String(36), ForeignKey("orders.id"))
    qty = Column(Integer, nullable=False)
    size = Column(String(2), nullable=False)

class PersistentProductsTable(Base):
    __tablename__ = "persistent_products"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    category_id = Column(String(36), ForeignKey("categories.id"))
    product_name = Column(String(255), nullable=False)
    description = Column(Text, nullable=False)
    is_new = Column(Boolean(), nullable=False)
    price = Column(Integer, nullable=False)

class CartsTable(Base):
    __tablename__ = "carts"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    user_id = Column(String(36), ForeignKey("users.id"))
    item_id = Column(String(36), ForeignKey("products.id"))
    qty = Column(Integer, nullable=False)
    size = Column(String(2), nullable=False)

    user = relationship("UsersTable", back_populates="cart", lazy="joined")
    products = relationship("ProductsTable", back_populates="carts", lazy="joined")

class BalancesTable(Base):
    __tablename__ = "balances"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(BigInteger(), primary_key=True, autoincrement=True)
    user_id = Column(String(36), ForeignKey("users.id"))
    balance = Column(BigInteger(), nullable=False)

    user = relationship("UsersTable", back_populates="balance", lazy="joined")

class ProductsTable(Base):
    __tablename__ = "products"
    __table_args__ = (
        UniqueConstraint('product_name', 'category_id', 'is_new'),
        {"mysql_engine": "InnoDB"},
    )

    id = Column(String(36), primary_key=True)
    category_id = Column(String(36), ForeignKey("categories.id"))
    product_name = Column(String(255), nullable=False)
    description = Column(Text, nullable=False)
    is_new = Column(Boolean(), nullable=False)
    price = Column(Integer, nullable=False)
    is_active = Column(Boolean(), nullable=False)

    category = relationship("CategoriesTable", back_populates="products", lazy="joined")
    carts = relationship("CartsTable", back_populates="products", lazy="joined")


class ShippingAddressTable(Base):
    __tablename__ = "shipping_address"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    user_id = Column(String(36), ForeignKey("users.id"))
    name = Column(String(255), nullable=False)
    phone_number = Column(String(14), nullable=False)
    address = Column(TEXT(), nullable=False)
    city = Column(String(255), nullable=False)

    user = relationship("UsersTable", back_populates="shipping_address", lazy="joined")

class CategoriesTable(Base):
    __tablename__ = "categories"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
    active = Column(Boolean(), nullable=False)

    products = relationship("ProductsTable", back_populates="category", lazy="joined")
    
class ImagesTable(Base):
    __tablename__ = "images"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    base64image = Column(MEDIUMTEXT(), nullable=False)

class BannersTable(Base):
    __tablename__ = "banners"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(String(36), primary_key=True)
    base64image = Column(MEDIUMTEXT(), nullable=False)
    title = Column(String(255), nullable=False)

class PersistentProductImagesTable(Base):
    __tablename__ = "persistent_product_images"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(BigInteger(), primary_key=True, autoincrement=True)
    product_id = Column(String(36), ForeignKey("persistent_products.id"))
    image_id = Column(String(36), ForeignKey("images.id"))

class ProductImagesTable(Base):
    __tablename__ = "product_images"
    __table_args__ = {
        "mysql_engine": "InnoDB"
    }

    id = Column(BigInteger(), primary_key=True, autoincrement=True)
    product_id = Column(String(36), ForeignKey("products.id"))
    image_id = Column(String(36), ForeignKey("images.id"))

def create_all_tables():
    Base.metadata.create_all(engine)