import os
from flask import request, current_app

from v1.middlewares.utils import generate_uuid
from v1.config import Config

def get_request(_key = None):
    try:
        json_req = request.get_json(True)
    except:
        json_req = {}
    requests = {**json_req, **request.args}
    if _key and (_key in requests.keys()):
        if requests[_key]:
            return requests[_key]
        else:
            return None
    elif _key and (_key not in requests.keys()):
        return None
    else:
        return requests

def get_headers(_key = None):
    if _key and (_key in request.headers.keys()):
        if request.headers[_key]:
            return request.headers[_key]
        else:
            return None
    elif _key and (_key not in request.headers.keys()):
        return None
    else:
        headers = {}
        for key in request.headers.keys():
            headers[key] = request.headers[key]
        if headers:
            return headers
        else:
            return None

def get_auth_token():
    token = get_headers("Authentication")
    return token

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'png', 'jpg', 'jpeg', 'gif'}

def get_files():
    if request.method == "POST":
        if "file" not in request.files:
            return None
        file = request.files["file"]
        if file.filename == "":
            return None
        if file and allowed_file(file.filename):
            ext = file.filename.rsplit('.', 1)[1].lower()
            filename = generate_uuid("images") + "." + ext
            return current_app.instance_path
            file.save(os.path.join(Config.UPLOAD_FOLDER, filename))
            return filename
    else:
        return None 