from functools import wraps

from v1.middlewares.auth import authenticate_token
from v1.middlewares.requests import get_auth_token

def auth_general():
    def _auth_general(f):
        @wraps(f)
        def __auth_general(*args, **kwargs):
            auth_info = authenticate_token(get_auth_token())
            if not auth_info:
                return {
                    "error": "Invalid API token"
                }, 401
            result = f(*args, **kwargs)
            return result
        return __auth_general
    return _auth_general
    
def auth_admin():
    def _auth_admin(f):
        @wraps(f)
        def __auth_admin(*args, **kwargs):
            auth_info = authenticate_token(get_auth_token())
            if not auth_info:
                return {
                    "error": "Invalid API token"
                }, 400
            else:
                if auth_info.is_admin == False:
                    return {
                        "error": "Resource access forbidden"
                    }, 403
            result = f(*args, **kwargs)
            return result
        return __auth_admin
    return _auth_admin