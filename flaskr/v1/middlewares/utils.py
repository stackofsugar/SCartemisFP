from uuid import uuid4
from random import choices
from string import digits, ascii_letters
from sqlalchemy.orm import Session
from sqlalchemy import select, update
from re import search

from v1.db.bootstrap import run_query, get_engine
from v1.db.models import ImagesTable, ProductsTable, ProductImagesTable, PersistentProductImagesTable

def assert_in_dict(dict: dict, *argv):
    """Assert in Dict
    Asserting a dict given strings, listing missing and empty keys
    bool returns True if nothing's missing or empty
    \nReturns bool, <missing>, <empty>
    """
    missing = []
    empty = []
    for arg in argv:
        if arg not in dict.keys():
            missing.append(arg)
        else:
            if not dict[arg]:
                empty.append(arg)
    if missing or empty:
        return False, missing, empty
    else:
        return True, missing, empty

def is_all_numbers(string: str):
    """Is all numbers
    Asserting if a string contains all numbers (0-9)
    """
    for elem in string:
        if not elem.isnumeric():
            return False
    return True

def has_upper(text: str):
    """Has uppercase character
    Asserting if a string contains any uppercase characters
    """
    for elem in text:
        if elem.isupper():
            return True
    return False

def has_lower(text: str):
    """Has lowercase character
    Asserting if a string contains any lowercase characters
    """
    for elem in text:
        if elem.islower():
            return True
    return False

def has_number(text: str):
    """Has any number
    Asserting if a string contains any number
    """
    for elem in text:
        if elem.isnumeric():
            return True
    return False

def check_unique(tablename: str, **kwargs):
    """Check database uniqueness"""
    not_unique = []
    for key, value in kwargs.items():
        result = run_query(f"""
            SELECT * FROM {tablename}
            WHERE {key} = '{value}'
        """)
        if result:
            not_unique.append(key)
    if not not_unique:
        return True, []
    else:
        return False, not_unique

def generate_random(len: int, precede = "", succede = "", chars = digits + ascii_letters):
    random = "".join(choices(chars, k=len))
    return precede + random + succede

def generate_uuid(tablename: str):
    while True:
        uuid = str(uuid4())
        is_unique, _ = check_unique(tablename, id=uuid)
        if is_unique:
            return uuid

def check_image(base64image: str):
    response = {}
    status = 400
    if len(base64image) >= 16777215:
        response = {
            "error": "Image is too big. Maximum upload size allowed is 64 MB"
        }
    else:
        regex = "^data:image/.*;base64$"
        test = str(base64image).split(",", 1)[0]
        if not search(regex, test):
            response = {
                "error": "Wrong image(s) format. Image should be in data URI base-64 format"
            }
        else:
            status = 200
            response = {
                "message": "Image OK"
            }
    return response, status

def upload_image(base64image: str):
    response = {}
    status = 201
    id = generate_uuid("images")
    with Session(get_engine()) as session:
        session.add(
            ImagesTable(id=id, base64image=base64image)
        )
        session.commit()
    response = {
        "message": "Image uploaded"
    }
    return response, status, id

def upload_check_image(base64image: str):
    response = {}
    status = 201
    id = None
    response, status = check_image(base64image)
    if status == 200:
        response, status, id = upload_image(base64image)
    return response, status, id

def get_images(product_obj, id_only: bool = False, persistent: bool = False):
    images = []
    with Session(get_engine()) as session:
        if persistent:
            productimages_obj = session.scalars(select(PersistentProductImagesTable).where(PersistentProductImagesTable.product_id == product_obj.id))
        else:
            productimages_obj = session.scalars(select(ProductImagesTable).where(ProductImagesTable.product_id == product_obj.id))
        for item in productimages_obj.all():
            if isinstance(item, ProductImagesTable) or isinstance(item, PersistentProductImagesTable):
                if not id_only:
                    images.append(f"/image/{item.image_id}")
                else:
                    images.append(item.image_id)
    return images

def calculate_shipping_price(total_price: int):
    regular_shipping = 0
    nextday_shipping = 0
    if total_price < 200:
        regular_shipping = 15 / 100 * total_price
    else:
        regular_shipping = 20 / 100 * total_price
    if total_price < 300:
        nextday_shipping = 20 / 100 * total_price
    else:
        nextday_shipping = 25 / 100 * total_price
    return [
        {
            "name": "regular",
            "price": int(regular_shipping),
        },
        {
            "name": "next day",
            "price": int(nextday_shipping),
        }
    ]