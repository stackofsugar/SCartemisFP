import jwt
from sqlalchemy import select
from sqlalchemy.orm import Session

from v1.db.models import UsersTable
from v1.db.bootstrap import get_engine
from v1.config import Config

def authenticate_token(token: str):
    response = {}
    decoded = {}
    valid_token = True
    try:
        decoded = jwt.decode(token, Config.SECRET_KEY, algorithms=["HS256"], issuer="fashion-campus-api")
    except:
        valid_token = False
    if valid_token:
        response = decoded["uid"]
        with Session(get_engine()) as session:
            user_obj = session.scalar(select(UsersTable).where(UsersTable.id == decoded["uid"]))
            if not isinstance(user_obj, UsersTable):
                response = None
            else:
                response = user_obj
    else:
        response = None
    return response